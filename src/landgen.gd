extends Node2D
#this script is in charge of generating the map and giving it collision

#var surface
onready var collider = $KinematicBody2D/surface_collider

#generation params
#all in pixels
var map_width = 3000
var noise_width = 100
var maxheight = 125
var minheight = 25

export(PoolVector2Array) var surface

func _draw():
	for i in range(surface.size() - 1):
		draw_line(surface[i], surface[i + 1], Color(255, 255, 255), 0)

# Called when the node enters the scene tree for the first time.
func _ready():

	#randomize()
	surface = PoolVector2Array()

	collider.set_build_mode(0)

	#var map_start = 500/2 - map_width/2
	#var map_end = 500/2 + map_width/2

	for i in $surface_nodes.get_children():
		surface.append(i.global_position)

#	proc gen
#	for i in range(map_start, map_end + 1, noise_width):
#		var point = Vector2(i, 500 - (randi() % (maxheight - minheight)) - minheight)
#		surface.append(point)


	#insert start and end points
#	surface.insert(0, Vector2(500/2 - map_width/2, 500))
#	surface.append(Vector2(500/2 + map_width/2, 500))


	#self.points = surface
	collider.polygon = surface

func _process(_delta):
	update()




