extends Node

var grid
var cdict: Dictionary

var tscale = 10.0
var kerning = 0.5

var canvas: CanvasItem

# Called when the node enters the scene tree for the first time.
func _ready():
	grid = PoolVector2Array([
		Vector2(  0, 0),
		Vector2(0.5, 0),
		Vector2(  1, 0),

		Vector2(  0, 1),
		Vector2(0.5, 1),
		Vector2(  1, 1),

		Vector2(  0, 2),
		Vector2(0.5, 2),
		Vector2(  1, 2)
	])

	var a = [6, 0, 2, 5, 3, 5, 8]
	var b = [0, 6, 7, 5, 3, 5, 1, 0] #wip
	var c = [2, 0, 6, 8]
	var d = [0, 6, 7, 5, 1, 0]
	var e = [2, 0, 3, 4, 3, 6, 8]
	var f = [2, 0, 3, 5, 3, 6]
	var g = [2, 0, 6, 8, 5, 4]
	var h = [0, 6, 3, 5, 2, 8]
	var i = [0, 2, 1, 7, 6, 8]
	var j = [2, 8, 6, 3]
	var k = [0, 6, 3, 2, 3, 8]
	var l = [0, 6, 8]
	var m = [6, 0, 7, 2, 8]
	var n = [6, 0, 8, 2]
	var o = [0, 6, 8, 2, 0]
	var p = [6, 0, 2, 5, 3]
	var q = [8, 2, 0, 6, 8, 4]
	var r = [6, 0, 2, 5, 3, 4, 8]
	var s = [2, 0, 3, 5, 8, 6]
	var t = [0, 2, 1, 7]
	var u = [0, 6, 8, 2]
	var v = [0, 7, 2]
	var w = [0, 6, 1, 8, 2]
	var x = [0, 8, 4, 2, 6]
	var y = [0, 4, 2, 4, 7]
	var z = [0, 2, 6, 8]

	#symbols
	var s_dash = [3, 5]
	var s_neg = [4, 5]
	var s_lparen = [2, 4, 8]
	var s_rparen = [0, 4, 6]

	#numbers
	var a0 = [2, 0, 6, 2, 8, 6]
	var a1 = [2, 8]
	var a2 = [0, 2, 5, 3, 6, 8]
	var a3 = [0, 2, 5, 3, 5, 8, 6]
	var a4 = [8, 2, 5, 3, 0]
	var a5 = [2, 0, 3, 5, 8, 6]
	var a6 = [2, 0, 6, 8, 5, 3]
	var a7 = [0, 2, 8]
	var a8 = [0, 6, 8, 5, 3, 5, 2, 0]
	var a9 = [5, 3, 0, 2, 8, 6]

	cdict = {
		" " : [],
		"a" : a,
		"b" : b,
		"c" : c,
		"d" : d,
		"e" : e,
		"f" : f,
		"g" : g,
		"h" : h,
		"i" : i,
		"j" : j,
		"k" : k,
		"l" : l,
		"m" : m,
		"n" : n,
		"o" : o,
		"p" : p,
		"q" : q,
		"r" : r,
		"s" : s,
		"t" : t,
		"u" : u,
		"v" : v,
		"w" : w,
		"x" : x,
		"y" : y,
		"z" : z,

		"0" : a0,
		"1" : a1,
		"2" : a2,
		"3" : a3,
		"4" : a4,
		"5" : a5,
		"6" : a6,
		"7" : a7,
		"8" : a8,
		"9" : a9,

		"-" : s_dash,
		"negative" : s_neg,
		"(" : s_lparen,
		")" : s_rparen
	}

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#should be moved to the draw functions so multiple hud objects can exist
func set_canvas(c):
	canvas = c

#takes string and draws it at the given location
func dstr(s, loc):
	var offset = 0
	for letter in s:
		for i in range(cdict[letter].size() - 1):
			canvas.draw_line(loc + (grid[cdict[letter][i]] * tscale) + Vector2(offset * tscale, 0), loc + (grid[cdict[letter][i + 1]] * tscale) + Vector2(offset * tscale, 0), Color(255, 255, 255), 0)

		offset += (1 + kerning)

func dnum(n, d, loc):
	var negative = n < 0
	n = abs(n)
	#final offset precalculated as number is drawn backwards
	var offset = (d - 1) * (1 + kerning)
	for _j in range(d):
		#current digit to be drawn
		var ones = n % 10
		#draw negative sign
		var sym = null
		if _j == d - 1 and negative:
			#set symbol  to negative sign
			sym = cdict["negative"]
		else:
			#set symbol to digit
			sym = cdict[str(ones)]

		#draw symbol
		for i in range(sym.size() - 1):
			canvas.draw_line(loc + (grid[sym[i]] * tscale) + Vector2(offset * tscale, 0), loc + (grid[sym[i + 1]] * tscale) + Vector2(offset * tscale, 0), Color(255, 255, 255))

		offset -= (1 + kerning)
		n = int(n / 10)

func calcw(s):
	return s * tscale + (s - 1) * (tscale * kerning)
