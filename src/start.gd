extends Node2D
var tscale = 10
var ofx = tscale + 0.5 * tscale
var ofy = 2 * tscale + 0.5 * tscale

# Called when the node enters the scene tree for the first time.
func _ready():
	Text.set_canvas(self)
	
func _draw():
	Text.dstr("sol lander", Vector2((get_viewport().size.x / 2) - (Text.calcw(10) / 2), get_viewport().size.y / 2 - ofy))
	Text.dstr("sta(r)t", Vector2((get_viewport().size.x / 2) - (Text.calcw(7) / 2), get_viewport().size.y / 2 + ofy))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_restart"):
		game_start()

	if Input.is_action_just_pressed("ui_quit"):
		get_tree().quit()
		
func game_start():
	get_tree().change_scene("res://scenes/base.tscn")

