extends KinematicBody2D

onready var world = get_parent()

export(PoolVector2Array) var vbody
var vexhaust: PoolVector2Array

#physics
var velocity = Vector2(0, 0)
var force = 1000
var mass = 10
var rots = 180 #in degrees per second

#camera and ground stuff
var ground_point = Vector2(0, 0)

#mechanics
var fuel = 30

# Called when the node enters the scene tree for the first time.
func _ready():
	vexhaust.append(Vector2(-4, 5))
	vexhaust.append(Vector2(0, 8))
	vexhaust.append(Vector2(4, 5))

	#starting velocity
	velocity = Vector2(0, 0)
	#self.global_rotation_degrees = -90

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	update()
	#print(velocity)

func _draw():
	#draw body
	for i in range(vbody.size()):
		draw_line(vbody[i], vbody[(i + 1) % vbody.size()], Color(255, 255, 255), 0)

	#draw exhaust
	for i in range(vexhaust.size()):
		draw_line(vexhaust[i], vexhaust[(i + 1) % vexhaust.size()], Color(255, 255, 255), 0)

var anim_acc = 0
var toggle = true
func _physics_process(delta):
	velocity -= world.gravity * delta

	anim_acc += delta
	if Input.is_action_pressed("ui_accept") and fuel > 0:
		var acc = force / mass
		var ang_acc = Vector2(0, -acc).rotated(self.global_rotation)
		velocity += ang_acc * delta

		#animate the exhaust firing
		#if anim_acc > 1.0/640:
		anim_acc = 0
		vexhaust[1].y = clamp(vexhaust[1].y + 1, 5, 8)

		#use fuel
		fuel -= 10 * delta

	else:
		vexhaust[1].y = 5
		anim_acc = 0
		#animate the exhaust stopping
#		if anim_acc > 1.0/60:



	if Input.is_action_pressed("ui_left"):
		self.rotation_degrees = self.rotation_degrees - (rots * delta)

	if Input.is_action_pressed("ui_right"):
		self.rotation_degrees = self.rotation_degrees + (rots * delta)

	#testing
	if Input.is_action_just_pressed("ui_end"):
		toggle = not toggle

	if not toggle:
		velocity = Vector2(0, 0)

	#velocity = move_and_slide(velocity)
	var col = move_and_collide(velocity * delta)
	if col:
		#need to check if the slope is too steep for failure as well
		if velocity.length() > 30:
			world.failure("too fast")
			#print("landed too fast")
		elif (self.global_rotation_degrees + 15 > 30) or (self.global_rotation_degrees - 15 < -30): #check the ship is well oriented (maybe add some "feet" colliders to be checked if they both hit the ground at the same time)
			world.failure("not vertically oriented")
			#print("not vertically oriented")
		elif abs(velocity.x) > 10: #check the ship is not sliding horizontally, the landing "slide" should be almost none
			world.failure("sliding too much")
			#print("sliding too much")
		elif acos(col.normal.dot(Vector2(0, -1))) > deg2rad(15): #check the landing slope is not too steep
			world.failure("slope too steep")
		else: #this should have a speed check?
			world.success()
			velocity = velocity.slide(col.normal) * (1 - world.friction)
		#print(col.collider.name)
		#ship_info()

	#TODO remove, only here for testing
#	if Input.is_action_pressed("ui_home"):
#		world.failure("manually triggered")

func ship_info():
	print("rot ", self.rotation_degrees)
	print("vel x:, ", velocity.x, ", vel y: ", velocity.y)
	print("vel: ", velocity.length())
