extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	Text.set_canvas(self)

func _draw():
	pass
	#Text.dstr("testing", Vector2(250 - Text.calcw("testing"), 250))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	#update()
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().paused = false
		visible = false
		set_process(false)

	if Input.is_action_just_pressed("ui_restart"):
		get_tree().reload_current_scene()
		get_tree().paused = false

	if Input.is_action_just_pressed("ui_quit"):
		get_tree().quit()

func _on_restart_button_down():
	get_tree().reload_current_scene()
	get_tree().paused = false


func _on_quit_button_down():
	get_tree().quit()
