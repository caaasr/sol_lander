extends Node2D

onready var cam = $camera
onready var target = $camera_target
var zoom_target = Vector2(1, 1)
onready var ship = $ship

#world settings
var gravity = Vector2(0, -10)
var drag = Vector2(0, 0)
var friction = 0.05
var wind = Vector2(0, 0)

#game
var timer = 30

# Called when the node enters the scene tree for the first time.
func _ready():
	cam.make_current()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_place_camera(delta)

	#ui updated
	$CanvasLayer/vHud.set_fuel(ceil($ship.fuel))
	$CanvasLayer/vHud.set_vel($ship.velocity)
	if timer > 0:
		timer -= delta
		$CanvasLayer/vHud.set_timer(ceil(timer))

	else:
		failure("OUT OF TIME")

	#pause game
	#TODO move this to vHud
	if Input.is_action_just_pressed("ui_cancel"):
		pass
#		$CanvasLayer/UI_pause.set_process(true)
#		#$CanvasLayer/vHud.set_state(1)
#		$CanvasLayer/UI_pause.visible = true
#
#		get_tree().paused = true

func _place_camera(delta):

	#move raycast to ship
	$RayCast2D.global_position = ship.global_position
	var ship_pos = ship.global_position
	var ground_pos = ship.global_position
	if $RayCast2D.is_colliding():
		ground_pos = $RayCast2D.get_collision_point()

	target.global_position = ship_pos.linear_interpolate(ground_pos, .5)

	#position camera
	cam.global_position.y = lerp(cam.global_position.y, target.global_position.y, 10 * delta)
	#cam.global_position.x = lerp(cam.global_position.x, target.global_position.x, 10 * delta)
	cam.global_position.x = target.global_position.x
	#zoom camera
	var dist = (ship_pos - ground_pos).length()

	#custom sizing function for zoom
	zoom_target = (Vector2(1.0, 1.0) * 5/8 * sqrt((1.0/100.0 * dist)))
	if $RayCast2D.is_colliding():
		zoom_target.x = clamp(zoom_target.x, .25, 5) #TODO reset min value
	else:
		zoom_target.x = 1
	var temp_zoom = cam.get_zoom().linear_interpolate(zoom_target, clamp(5 * delta, 0, 1))
	cam.set_zoom(Vector2(temp_zoom.x, temp_zoom.x))


#TODO migrage these to the vHud script
func failure(reason):
	$CanvasLayer/vHud.gstate = $CanvasLayer/vHud.state.FAILURE
	$CanvasLayer/vHud.reason = reason
#	$CanvasLayer/UI_failure.set_process(true)
#	$CanvasLayer/UI_failure.visible = true
#	$CanvasLayer/UI_failure/VBoxContainer/reason.text = reason
	get_tree().paused = true

func success():
	$CanvasLayer/vHud.gstate = $CanvasLayer/vHud.state.SUCCEED
	get_tree().paused = true
#	#print("PLAYER successfully landed SHIP")
#	$CanvasLayer/UI_success.set_process(true)
#	$CanvasLayer/UI_success.visible = true
#	var score = (timer * ship.fuel) / ship.velocity.length()
#	$CanvasLayer/UI_success/VBoxContainer/reason.text += str(abs(ceil(score)))
#	get_tree().paused = true
