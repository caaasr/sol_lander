extends Node2D

var tscale = 10.0
var kerning = 0.5

var in_f: int = 0
var in_t: int = 0
var in_v: Vector2

var symbols: Array

#should get moved to the base script
enum state {
	RUNNING,
	PAUSED,
	FAILURE,
	SUCCEED
}

var gstate

var ofx = tscale + 0.5 * tscale
var ofy = 2 * tscale + 0.5 * tscale

var reason = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	Text.set_canvas(self)
	gstate = state.RUNNING

func _draw():
	match gstate:
		state.RUNNING:
			Text.dstr("timer", Vector2(tscale, tscale + 0 * ofy))
			Text.dstr("fuel",  Vector2(tscale, tscale + ofy))

			Text.dnum(in_t, 3, Vector2(tscale + 6 * ofx, tscale))
			Text.dnum(in_f, 3, Vector2(tscale + 6 * ofx, tscale + ofy))

		state.PAUSED:
			Text.dstr("paused", Vector2((get_viewport().size.x / 2) - (Text.calcw(6) / 2), get_viewport().size.y / 2))
			Text.dstr("(r)estart", Vector2((get_viewport().size.x / 2) - (Text.calcw(9) / 2), get_viewport().size.y / 2 + ofy))
			Text.dstr("(q)uit", Vector2((get_viewport().size.x / 2) - (Text.calcw(6) / 2), get_viewport().size.y / 2 + ofy * 2))
			
		state.FAILURE:
			Text.dstr("failure", Vector2((get_viewport().size.x / 2) - (Text.calcw(7) / 2), get_viewport().size.y / 2))
			Text.dstr(reason, Vector2((get_viewport().size.x / 2) - (Text.calcw(len(reason)) / 2), get_viewport().size.y / 2 + ofy))
			Text.dstr("(r)estart", Vector2((get_viewport().size.x / 2) - (Text.calcw(9) / 2), get_viewport().size.y / 2 + ofy * 2))

		state.SUCCEED:
			Text.dstr("success", Vector2((get_viewport().size.x / 2) - (Text.calcw(7) / 2), get_viewport().size.y / 2))
			Text.dstr("(r)estart", Vector2((get_viewport().size.x / 2) - (Text.calcw(9) / 2), get_viewport().size.y / 2 + ofy))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	update()

	#handle menu inputs here
	match gstate:
		state.RUNNING:
			if Input.is_action_just_pressed("ui_cancel"):
				gstate = state.PAUSED
				get_tree().paused = true
				
		state.PAUSED:
			if Input.is_action_just_pressed("ui_cancel"):
				gstate = state.RUNNING
				get_tree().paused = false
				
			if Input.is_action_just_pressed("ui_restart"):
				get_tree().reload_current_scene()
				get_tree().paused = false
				
			if Input.is_action_just_pressed("ui_quit"):
				get_tree().quit()
		
		state.FAILURE:	
			if Input.is_action_just_pressed("ui_restart"):
				get_tree().reload_current_scene()
				get_tree().paused = false
			
		state.SUCCEED:
			if Input.is_action_just_pressed("ui_restart"):
				get_tree().reload_current_scene()
				get_tree().paused = false
				
func set_state(s):
	gstate = s

func set_fuel(val):
	in_f = val

func set_timer(val):
	in_t = val

func set_vel(val):
	in_v = val
